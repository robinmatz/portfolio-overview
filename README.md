# Robin Matz: Portfolio Overview

This is the landing page of my personal portfolio. It contains links to example projects
created using different automation frameworks.

## SoapUI
[httpbin.org](https://gitlab.com/a7745/soapui/httpbin.org) - a small API test automation
project centering around the publicly available [httpbin.org](https://httpbin.org/) project.
